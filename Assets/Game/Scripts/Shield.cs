﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Game.Scripts
{
    public class Shield : MonoBehaviour
    {
        [SerializeField]
        private float _health = 5;

        [SerializeField]
        private LayerMask _projectileMask;

        [SerializeField]
        private LayerMask _enemyMask;

        [SerializeField]
        private float _movementAmount = 6f;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (MaddoUnityTools.UsefulTools.UsefulTools.IsLayerInMask(collision.gameObject.layer, _projectileMask))
            {
                var p = collision.GetComponent<Projectile>();
                if (p)
                {
                    if (p.Affiliation == Projectile.Affiliations.Player)
                    {
                        this.transform.Translate(0, _movementAmount, 0);

                    }
                    else
                    {
                        
                    }
                    this._health -= p.Damage;
                    Destroy(p.gameObject);
                }
            }
            else if (MaddoUnityTools.UsefulTools.UsefulTools.IsLayerInMask(collision.gameObject.layer, _enemyMask))
            {
                Enemy e = collision.GetComponent<Enemy>();
                if (e)
                {
                    e.Hurt(1);
                }
                this._health -= 1;

            }

            if (this._health <= 0)
            {
                Destroy(this.gameObject);
            }
        }


    }
}
